#!/bin/sh

###
### @(#) install-urw.sh -- Installing the URW font package into the local TEXMF structure
###
### Time-stamp:  <Tue Dec 22 14:21:00 CET 1998 -- Jan Braun>
###
###
### @(#) $Id: $
### @(#) $Keywords: URW Fonts $
###
###
###          File: /home/tmjb/Privat/FSFE/install-urw.sh
###       Project: FSFE letterhead
###   Description: Installing the URW font package into your local tree
###                of the TDS compliant TEXMF file structure.  This
###                file assumes, that the URW pakcage was downloaded
###                from CTAN and is available as /tmp/urwvf.zip, for instance
###                from ftp://tug.ctan.org/pub/tex-archive/fonts/psfonts/urwvf.zip
###
###                If it is not available, it will try to wget it.
###
###                It decompresses that file and moves all files to their
###                appropiate places.  Please adjust the root of your
###                local TEXMF tree in accordance to you global TeX
###                configuration.  In case you happen to use teTeX,
###                have a look in /etc/texmf.cnf (or in DEBIAN
###                notation: /etc/texmf/texmf.cnf).
###
###                Maybe you have to correct the ownership and access
###                rights of the newly created files and directories.
###       Version: $Revision: $
###        Author: tmjb -- Jan Braun <Braun@rz.tu-clausthal.de>
###    Maintainer: tmjb -- Jan Braun <Braun@rz.tu-clausthal.de>
### Creation-Date: Mon Mar 12 2007 -- Jan Braun <Braun@rz.tu-clausthal.de>
###     Copyright: (c) 2007 Jan Braun
###       Licence: GNU General Public Licence v2 or later
###

### ------------------------------------------------------ &Change Log ---
###
### $Log: $


### ======================================================== &Preamble ===

## Please, don't forget to adjust these values.
## Maybe you have to start this script as root or at least with
## appropriate rights.

umask 002

## Local TDS compliant TEXMF tree root
TEXMFLOCAL=/usr/local/share/texmf

## Source package to install
SRCDIR=/tmp
SRC=urwvf.zip
WWWSRC=ftp://tug.ctan.org/pub/tex-archive/fonts/psfonts/urwvf.zip

### ============================================================ &Main ===

## cd to the SRCDIR and uncompress the source ZIP
cd $SRCDIR
[ -e $SRC ] || wget $WWWSRC
unzip $SRC

## create the needed directories
mkdir -p $TEXMFLOCAL/fonts/tfm/urw
mkdir -p $TEXMFLOCAL/fonts/vf/urw/
mkdir -p $TEXMFLOCAL/fonts/afm/urw/
mkdir -p $TEXMFLOCAL/fonts/type1/urw/
mkdir -p $TEXMFLOCAL/fonts/map/fontname/
mkdir -p $TEXMFLOCAL/tex/latex/urw/
mkdir -p $TEXMFLOCAL/dvips/misc/

## mv the contents of the ZIP file into their target directories
cd urwvf
mv ./tfm/* $TEXMFLOCAL/fonts/tfm/urw
mv ./vf/* $TEXMFLOCAL/fonts/vf/urw/
mv ./afm/* $TEXMFLOCAL/fonts/afm/urw/
mv ./pfa/* $TEXMFLOCAL/fonts/type1/urw/
mv ./latex2e/* $TEXMFLOCAL/tex/latex/urw/
cat ./fontname/urw.map >> $TEXMFLOCAL/fonts/map/fontname/urw.map
cat ./dvips/psfonts.map >> $TEXMFLOCAL/dvips/misc/psfonts.map

## Don't forget to update the texmf database
texhash
## on DEBIAN-like systems you can run
update-texmf
update-updmap

## clean up SRCDIR
cd $SRCDIR
rm -rf urwvf


### ============================================================= &EOF ===
